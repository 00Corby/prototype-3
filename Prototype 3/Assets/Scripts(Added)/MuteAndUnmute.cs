// Mutes-Unmutes the sound from this object each time the user presses space.
using UnityEngine;
using System.Collections;

public class MuteAndUnmute : MonoBehaviour
{ 
    AudioSource m_MyAudioSource;

    float m_MySliderValue;

    void Start()
    {
        //Initiate the Slider value to zero
        m_MySliderValue = 0f;
        //Fetch the AudioSource from the GameObject
        m_MyAudioSource = GetComponent<AudioSource>();
        //Play the AudioClip attached to the AudioSource on startup
        m_MyAudioSource.Play();
        m_MyAudioSource.volume = m_MySliderValue;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_MySliderValue = 1f;
            m_MyAudioSource.volume = m_MySliderValue;
        }
    }
}